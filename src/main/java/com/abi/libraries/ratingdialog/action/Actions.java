package com.abi.libraries.ratingdialog.action;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.abi.libraries.ratingdialog.R;

public class Actions {
    
    public static void sendEmail(Activity activity, String feature) {
        String featureLabel = activity.getResources().getString(R.string.send_email_feature_label);
        String finalTextSubject = activity.getResources().getString(R.string.send_email_final_text_subject);
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"julio.costa@ab-inbev.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Feedback - " + featureLabel + " " + feature + " - " + finalTextSubject);
        i.putExtra(Intent.EXTRA_TEXT, "");
        try {
            activity.startActivity(Intent.createChooser(i, ""));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, activity.getResources().getString(R.string.email_client_not_found), Toast.LENGTH_SHORT).show();
        }
    }
}

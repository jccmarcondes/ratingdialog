package com.abi.libraries.ratingdialog.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.abi.libraries.ratingdialog.R;
import com.abi.libraries.ratingdialog.action.Actions;

public class RatingDialog {
    
    private Activity mActivity;
    private Dialog mDialog;
    private String mFeature;
    private View.OnClickListener lowRatingListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mDialog != null) {
                mDialog.dismiss();
            }
            Actions.sendEmail(mActivity, mFeature);
        }
    };
    
    public RatingDialog(Activity activity, String feature) {
        mActivity = activity;
        mFeature = feature;
    }
    
    public void show() {
        
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        
        View viewDialog = LayoutInflater.from(mActivity).inflate(R.layout.dialog_rating, null);
        RatingBar ratingBar = viewDialog.findViewById(R.id.rating_bar);
        
        final View containerRating = viewDialog.findViewById(R.id.container_ratting);
        final View containerLow = viewDialog.findViewById(R.id.container_low_rating);
        final TextView btnOkCancel = viewDialog.findViewById(R.id.btn_cancel);
        final TextView btnOkCancel2 = viewDialog.findViewById(R.id.btn_cancel2);
        final TextView btnSendEmail = viewDialog.findViewById(R.id.btn_send_email);
        
        builder.setView(viewDialog);
        mDialog = builder.create();
        mDialog.setCancelable(false);
        ratingBar.setOnRatingBarChangeListener((ratingBar1, v, b) -> {
            if (v > 3.0f) {
                mDialog.dismiss();
            } else {
                containerLow.setVisibility(View.VISIBLE);
                containerRating.setVisibility(View.GONE);
            }
        });
        
        View.OnClickListener cancelClickListener = view -> {
            mDialog.dismiss();
        };
        
        btnOkCancel.setOnClickListener(cancelClickListener);
        btnOkCancel2.setOnClickListener(cancelClickListener);
        btnSendEmail.setOnClickListener(lowRatingListener);
        
        mDialog.show();
        
    }
    
    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
